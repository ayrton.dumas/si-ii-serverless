const TelegramBot = require('node-telegram-bot-api');
const request = require('request');

// Replace the value below with the Telegram token you receive from @BotFather
const token = '660028004:AAE26qZcRlPoyYP4yvMF5VPRRfkwErVwVXI';

// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(token, {polling: true});

// Matches "/help"
bot.onText(/\/help/, (msg) => {
  // 'msg' is the received message from Telegram
  const chatId = msg.chat.id;
  var help = 'Hey. I have 3 commands for now:\n'
  help += '"/help": you know that already\n'
  help += '"/echo msg": i will just repeat what you just said\n'
  help += '"/subscribe": i will subscribe you to a list of subscribers\n'
  help += '"/broadcast msg": i will send your message msg to every user subscribed =D #spam'
  bot.sendMessage(chatId, help);
});

// Matches "/echo [whatever]"
bot.onText(/\/echo (.+)/, (msg, match) => {
  // 'msg' is the received message from Telegram
  // 'match' is the result of executing the regexp above on the text content of the message

  const chatId = msg.chat.id;
  const resp = match[1]; // the captured "whatever"

  // send back the matched "whatever" to the chat
  bot.sendMessage(chatId, resp);
});

// Matches "/subscribe"
bot.onText(/\/subscribe/, (msg) => {
  const chatId = msg.chat.id;
  var options = {
    uri: 'https://wt-df053a2cf4fc464e6c28e7a1acc8c917-0.sandbox.auth0-extend.com/mongo-set-user',
    method: 'POST',
    json: {
      'user': msg.chat
    }
  }

  console.log(msg.chat)
  request(options, function (error, response, body) {
    console.log('error:', error);
    console.log('statusCode:', response && response.statusCode);
    console.log('body:', body);
    bot.sendMessage(chatId, body.status)
  });
});

// Matches "/broadcast [whatever]"
bot.onText(/\/broadcast (.+)/, (msg, match) => {
  const chatId = msg.chat.id;
  const resp = match[1];
  console.log(resp);

  var options = {
    uri: 'https://wt-df053a2cf4fc464e6c28e7a1acc8c917-0.sandbox.auth0-extend.com/mongo-get-user',
    method: 'POST',
    json: {
      'user': msg.chat,
      'msg': resp
    }
  }
  request(options, function (error, response, body) {
    console.log('error:', error);
    console.log('statusCode:', response && response.statusCode);
    console.log('body:', body.users);

    for (var i = body.users.length - 1; i >= 0; i--) {
      // send the message to every users
      bot.sendMessage(body.users[i].telegram_id, resp)
    }
    
    bot.sendMessage(chatId, 'Message successfully broadcasted to ' + body.users.length + ' users :)')
  });
});

// Listen for any kind of message.
bot.on('message', (msg) => {
  const chatId = msg.chat.id;
  // send a message to the chat acknowledging receipt of their message
  bot.sendMessage(chatId, 'Received your message');
});