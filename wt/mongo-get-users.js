var MongoClient = require('mongodb@2.2.33').MongoClient;

module.exports = function (context, cb) {
  var url = 'mongodb://tonayr:Stitch1997@ds149489.mlab.com:49489/si-ii-serverless';
  MongoClient.connect(url, function (err, db) {
    if(err) return cb(err);

    db.collection('users').find({}).toArray(function(err, result) {
      if (err) throw err;
      console.log(result);


      var data = [];
      for (var i = 0; i < result.length; i++) { 
        data.push(result[i]);
      }

      db.collection('msg').insertOne(

        {
          first_name: context.body.user.first_name,
          last_name: context.body.user.last_name,
          username: context.body.user.username,
          telegram_id: context.body.user.id,
          msg: context.body.msg,
          count: data.length,
          time: new Date()
        }

      )


      db.close();
      cb(null, { users: data, status: 'ok' });
    });
  });
};