var MongoClient = require('mongodb@2.2.33').MongoClient;

module.exports = function (context, cb) {
  var url = 'mongodb://tonayr:Stitch1997@ds149489.mlab.com:49489/si-ii-serverless';
  MongoClient.connect(url, function (err, db) {
    if(err) return cb(err);

    //console.log(context.body);
    if (!('body' in context) || !('user' in context.body)) {
      cb(null, { status: 'Error - fill the body of the request' });
    } else {
      db.collection('users').update(
        {
          telegram_id:context.body.user.id
        },
        {
          first_name: context.body.user.first_name,
          last_name: context.body.user.last_name,
          username: context.body.user.username,
          telegram_id: context.body.user.id,
          type: context.body.user.type,
          time: new Date()
        }
        {upsert: true, safe: false},

      );}
      cb(null, { status: 'User correctly subscribed' });
  });
};